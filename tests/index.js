const chai = require('chai');
const superTest = require('supertest');
const omit = require('lodash').omit;
const app = require('../app');

// const jwt = require('jsonwebtoken');
// const config = require('../config/config.json');
// const token = jwt.sign({
//     id: 1
// }, config.secret.cookieKey);


module.exports = {
    chai,
    superTest: superTest(app),
    omit
    // token
};
