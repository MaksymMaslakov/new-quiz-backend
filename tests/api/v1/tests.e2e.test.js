const {  chai, superTest, omit } = request('../../index')

const tests = [
    {
        id: 1,
        user_id: 88,
        title: "Test 1",
        questions: [
            {
                id: 1,
                type: "radio",
                category: "Web-developing",
                text: "Witch instruction install all dependencies?",
                score_value: 4,
                test_id: 1,
                options: [
                    {
                        id: 1,
                        text: "npm run serve",
                        is_right: false,
                        question_id: 1
                    },
                    {
                        id: 2,
                        text: "npm run build",
                        is_right: false,
                        question_id: 1
                    },
                    {
                        id: 3,
                        text: "npm install",
                        is_right: false,
                        question_id: 1
                    }
                ]
            }
        ]
    }
]

describe('Tests', () => {
    describe('POST api/v1/tests', () => {
        it('should create new test', () => {
             return superTest
                 .post('api/v1/tests')
                 .send(tests[0])
                 .set('Accept', 'application/json')
                 .expect('Content-Type', /json/)
                 .expect(201)
                 .then( res => {
                     chai.expect(omit(res.body, [ 'created_at', 'updated_at']))
                         .to.deep.equal(tests[0])
                 })
        })
    });

    describe('GET api/v1/tests', () => {
        it('should return all tests', () => {
            return superTest
                .get('api/v1/tests')
                .expect('Content-Type', /json/)
                .expect(200)
        })
    })

    describe('GET api/v1/tests/:id', () => {
        it('should return test by id', () => {
            return superTest
                .get('api/v1/tests/1')
                .expect('Content-Type', /json/)
                .expect(200)
                .then( res => {
                    chai.expect(omit(res.body, [ 'created_at', 'updated_at']))
                        .to.deep.equal(tests.find( test => test.id === 1)) // test data
                })
        })
    })

    describe('GET api/v1/tests/:id?passing="true"', () => {
        it('should return test by id for passing this test', () => {
            return superTest
                .get('api/v1/tests/1?passing="true"')
                .expect('Content-Type', /json/)
                .expect(200)
                .then( res => {

                    let testForPassing = tests.find( test => test.id === 1)
                    testForPassing.questions.forEach( question => {
                        question.options = question.options.map( option => omit(option, ['is_right']))
                    })

                    chai.expect(omit(res.body, [ 'created_at', 'updated_at']))
                        .to.deep.equal(testForPassing)
                })
        })
    })

    describe('PUT api/v1/tests/:id', () => {
        let updatesTest = tests[0]
        updatesTest.title = 'new title'

        it('should update test by id', () => {
            return superTest
                .put('api/v1/tests/1')
                .send(updatesTest)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .then( res => {
                    chai.expect(omit(res.body, [ 'created_at', 'updated_at']))
                        .to.deep.equal(updatesTest)
                })
        })
    })

    describe('DELETE api/v1/tests/:id', () => {
        it('should delete test by id', () => {
            return superTest
                .delete('api/v1/tests/1')
                .expect(200)
                .then( res => {
                    chai.expect(omit(res.body, [ 'created_at', 'updated_at']))
                        .to.deep.equal(tests.find( test => test.id === 1))
                })
        })
    })
})